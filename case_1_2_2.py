'''
Case Study 1.2.1 - Spectral Clustering News Stories
Jake Ullman
'''

import ssl
from bs4 import BeautifulSoup as soup
from urllib.request import urlopen
from goose3 import Goose
from tqdm import tqdm
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import SpectralClustering
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.preprocessing import LabelEncoder

'''Defining functions to enable ingesting data easily'''


def scape_news_links(xml_news_url):
    '''
    Provide a url which will return an XML file to parse. Returns the links to news stories.

    Params
    ------
    xml_news_url: string
        A URL which will return
    '''

    context = ssl._create_unverified_context()
    Client = urlopen(xml_news_url, context=context)
    xml_page = Client.read()
    Client.close()

    soup_page = soup(xml_page, "xml")

    news_list = soup_page.findAll("item")

    return [news.link.text for news in news_list]


def scrape_news_articles(topics=[]):
    if topics == []:
        topics = ['WORLD', 'SPORTS', 'TECHNOLOGY', 'BUSINESS', 'ENTERTAINMENT', 'SCIENCE', 'HEALTH']
    stem = "https://news.google.com/news/rss/headlines/section/topic/"
    g = Goose()
    articles = []
    for topic in topics:
        links = scape_news_links(stem + topic)
        for link in tqdm(links):
            try:
                article = g.extract(url=link)
                articles.append({
                    'title': article.title,
                    'tag': topic,
                    'text': article.cleaned_text
                })
            except:
                pass
    return articles


'''Stops the functions from running every time'''
pull_new = False  # set to True if data should load in again
if pull_new:
    data = scrape_news_articles()
    df = pd.DataFrame(data)
    df.to_pickle('./google_news_data/raw_articles.pickle')
    df.to_csv('./google_news_data/raw_articles.csv')
else:
    df = pd.read_pickle('./google_news_data/raw_articles.pickle')
encoder = LabelEncoder()
df['tag'] = encoder.fit_transform(df['tag'])
# print(df.head())

'''Transforming the data with TF-IDF (how relevant a word is to a document)'''
stop_words = ["a", "about", "above", "above", "across", "after", "afterwards", "again", "against", "all", "almost", "alone", "along", "already", "also","although","always","am","among", "amongst", "amoungst", "amount",  "an", "and", "another", "any","anyhow","anyone","anything","anyway", "anywhere", "are", "around", "as",  "at", "back","be","became", "because","become","becomes", "becoming", "been", "before", "beforehand", "behind", "being", "below", "beside", "besides", "between", "beyond", "bill", "both", "bottom","but", "by", "call", "can", "cannot", "cant", "co", "con", "could", "couldnt", "cry", "de", "describe", "detail", "do", "done", "down", "due", "during", "each", "eg", "eight", "either", "eleven","else", "elsewhere", "empty", "enough", "etc", "even", "ever", "every", "everyone", "everything", "everywhere", "except", "few", "fifteen", "fify", "fill", "find", "fire", "first", "five", "for", "former", "formerly", "forty", "found", "four", "from", "front", "full", "further", "get", "give", "go", "had", "has", "hasnt", "have", "he", "hence", "her", "here", "hereafter", "hereby", "herein", "hereupon", "hers", "herself", "him", "himself", "his", "how", "however", "hundred", "ie", "if", "in", "inc", "indeed", "interest", "into", "is", "it", "its", "itself", "keep", "last", "latter", "latterly", "least", "less", "ltd", "made", "many", "may", "me", "meanwhile", "might", "mill", "mine", "more", "moreover", "most", "mostly", "move", "much", "must", "my", "myself", "name", "namely", "neither", "never", "nevertheless", "next", "nine", "no", "nobody", "none", "noone", "nor", "not", "nothing", "now", "nowhere", "of", "off", "often", "on", "once", "one", "only", "onto", "or", "other", "others", "otherwise", "our", "ours", "ourselves", "out", "over", "own","part", "per", "perhaps", "please", "put", "rather", "re", "same", "see", "seem", "seemed", "seeming", "seems", "serious", "several", "she", "should", "show", "side", "since", "sincere", "six", "sixty", "so", "some", "somehow", "someone", "something", "sometime", "sometimes", "somewhere", "still", "such", "system", "take", "ten", "than", "that", "the", "their", "them", "themselves", "then", "thence", "there", "thereafter", "thereby", "therefore", "therein", "thereupon", "these", "they", "thick", "thin", "third", "this", "those", "though", "three", "through", "throughout", "thru", "thus", "to", "together", "too", "top", "toward", "towards", "twelve", "twenty", "two", "un", "under", "until", "up", "upon", "us", "very", "via", "was", "we", "well", "were", "what", "whatever", "when", "whence", "whenever", "where", "whereafter", "whereas", "whereby", "wherein", "whereupon", "wherever", "whether", "which", "while", "whither", "who", "whoever", "whole", "whom", "whose", "why", "will", "with", "within", "without", "would", "yet", "you", "your", "yours", "yourself", "yourselves", "the"]
vectorizer = TfidfVectorizer(stop_words=stop_words)
vectorized = vectorizer.fit_transform(df['text'])
dense = vectorized.todense()
# print(vectorized.shape)

'''Calculating the laplacian of a graph representing the data'''
model = SpectralClustering(n_clusters=7,
                           affinity="nearest_neighbors",
                           n_neighbors = 30)
model.fit(vectorized)
base_predictions = model.labels_
# print(base_predictions)

'''Reducing the dimensions to 2 using PCA'''
pca = PCA(n_components=2)
p_embedding = pca.fit_transform(dense)
f, ax = plt.subplots(1, 2, figsize=(12, 8))
ax[0].scatter(p_embedding[:, 0],
              p_embedding[:, 1],
              c=df['tag'])
ax[0].set_title("PCA Embedding with Base Truth")
ax[1].scatter(p_embedding[:, 0],
              p_embedding[:, 1],
              c=base_predictions)
ax[1].set_title("PCA Embedding with Spectral Clustering")
plt.show()

'''Reducing the dimension to 2 using T-SNE'''
tsne = TSNE(n_components=2, perplexity=20.0, early_exaggeration=12.0,
            learning_rate=300.0, n_iter=1000, n_iter_without_progress=300,
            verbose=0)
t_embedding = tsne.fit_transform(dense)
f, ax = plt.subplots(1, 2, figsize=(12, 8))
ax[0].scatter(t_embedding[:, 0],
              t_embedding[:, 1],
              c=df['tag'])
ax[0].set_title("T-SNE Embedding with Base Truth")
ax[1].scatter(t_embedding[:, 0],
              t_embedding[:, 1],
              c=base_predictions)
ax[1].set_title("T-SNE Embedding with Spectral Clustering")
plt.show()

'''Checking title predictions'''
df['predictions'] = base_predictions
df.to_csv('./google_news_data/base_predictions.csv')
print(df.head())

'''Using metric learning or base truths to evaluated the clustering'''
error = df.groupby('tag').agg({'predictions':'var'}).reset_index()
error['labels'] = encoder.inverse_transform(error['tag'])
print(error)

