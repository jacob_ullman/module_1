'''
Case Study 1.1.2 - LDA Analysis
Jake Ullman
'''

import requests
from bs4 import BeautifulSoup
import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from wordcloud import WordCloud
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.decomposition import LatentDirichletAllocation as LDA
from pyLDAvis import sklearn as sklearn_lda


'''Gathering faculty data from department website'''
try:
    page = requests.get('https://www.eecs.mit.edu/people/faculty-advisors/')
    faculty = BeautifulSoup(page.content, 'html.parser')
except ValueError:
    print('Error grabbing elements from page', ValueError)

# print(soupMembers)

'''Cleaning up data'''
counter = 0
names = []
partially_cleaned_list = faculty.find_all("div", class_="views-field views-field-title")
for person in partially_cleaned_list:
    name = person.span.a
    if name is None:
        name = person.span
    names.append(name.text)
    counter += 1

'''Cleaning up data more and inserting into search'''
name_df = pd.DataFrame(names, columns=['name'])
print(name_df.iloc[11])
name_df[['first_name', 'last_name']] = \
    name_df['name'].loc[name_df['name'].str.split().str.len() == 2].str.split(expand=True)
name_df['find_url'] = 'https://arxiv.org/search/?query=' + name_df['first_name'] + '+' + \
                      name_df['last_name'] + '&searchtype=all&abstracts=show&order=-announced_date_first&size=50'

urlAbstract = name_df.iloc[11][3]  # select which person to analyze

try:
    pageAbstract = requests.get(urlAbstract)
    soupAbstract = BeautifulSoup(pageAbstract.content, 'html.parser')
except ValueError:
    print('Could not get elements from page', ValueError )

'''Obtaining the abstract from the search'''
counter_abstract = 0
abstract_title = []
abstract_main = []
listAbstractHtml = soupAbstract.find_all(class_="arxiv-result")

for item in listAbstractHtml:
    title = item.find(class_="title is-5 mathjax").text.strip(' \n\t')
    abstract_title.append(title)
    tokenMore = 'More\n\n\n        '
    tokenLess = '\n        △ Less\n\n'
    abstract = item.find(class_="abstract mathjax").text
    init = abstract.find(tokenMore)
    end = abstract.find(tokenLess)
    abstract_main.append(abstract[init + len(tokenMore):end])
    counter_abstract += 1

'''Combining lists and turning into dataframe'''
listTitle_Abstract = {'titles': abstract_title, 'abstract': abstract}
abstractAndTitles_df = pd.DataFrame(listTitle_Abstract, columns=['titles', 'abstract'])
# print(abstractAndTitles_df)

'''Create a word cloud of common words'''
long_string = ','.join(list(abstractAndTitles_df['titles'].values))
long_string += ','.join(list(abstractAndTitles_df['abstract'].values))
word_cloud = WordCloud(background_color="white", max_words=10000, contour_width=3, contour_color='steelblue')
word_cloud.generate(long_string)
plt.imshow(word_cloud, interpolation='bilinear')
plt.axis("off")
plt.show()

'''Generating graph of 10 most common words w/o stop words'''
sns.set_style('whitegrid')
count_vectorizer = CountVectorizer(stop_words='english')
count_data = count_vectorizer.fit_transform(long_string.split())
words = count_vectorizer.get_feature_names()
total_counts = np.zeros(len(words))
for t in count_data:
    total_counts += t.toarray()[0]

count_dict = (zip(words, total_counts))
count_dict = sorted(count_dict, key=lambda x: x[1], reverse=True)[0:10]
words = [w[0] for w in count_dict]
counts = [w[1] for w in count_dict]
x_pos = np.arange(len(words))
plt.subplot(title='Most Common Words')
sns.barplot(x_pos, counts)
plt.xticks(x_pos, words, rotation=90)
plt.xlabel('words')
plt.ylabel('counts')
plt.show()

'''Generating list of topics found via LDA'''
number_topics = 5
number_words = 10
lda = LDA(n_components=number_topics, n_jobs=-1)
lda.fit(count_data)
print("Topics found via LDA:")
words = count_vectorizer.get_feature_names()
lda_counter = 1
for topic_idx, topic in enumerate(lda.components_):
    print("\nTopic #%d:" % lda_counter)
    print(" ".join([words[i] for i in topic.argsort()[:-number_words - 1:-1]]))
    lda_counter += 1

'''Create an interactive topic model visualization'''
LDAvis_prepared = sklearn_lda.prepare(lda, count_data, count_vectorizer)
sklearn_lda.pyLDAvis.show(LDAvis_prepared)
