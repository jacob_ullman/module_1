'''
Case Study 1.2.1 - Identifying Faces
Jake Ullman
'''

import os
from glob import glob
from sklearn.preprocessing import normalize
from sklearn.decomposition import PCA
import matplotlib.pyplot as plt
import numpy as np
from PIL import Image
image_dir = './instructor_photos'
n_instructors = 14

'''Defining functions to obtain images easier'''


def get_image(image_path, width, height, mode):
    image = Image.open(image_path)

    return np.array(image.convert(mode))


def get_batch(image_files, width, height, mode):
    data_batch = np.array(
        [get_image(sample_file, width, height, mode) for sample_file in image_files]).astype(np.float32)

    return data_batch


'''Grabbing images and plotting'''
instructor_images = get_batch(glob(os.path.join(image_dir, '*.jpg'))[:n_instructors], 300, 300, 'L')
fig = plt.figure(figsize=(8, 6))
for i in range(n_instructors):
    ax = fig.add_subplot(3, 5, i + 1, xticks=[], yticks=[])
    ax.imshow(instructor_images[i], cmap='gray')
plt.show()

'''Normalizing faces and applying PCA'''
face_vector = []
for i in instructor_images:
    face_image = i.reshape(300 * 300, )
    face_vector.append(face_image)

face_vector = np.asarray(face_vector)
model = PCA(n_components=n_instructors)
normalized = normalize(face_vector)
model.fit(normalized)
normalized_transformed = model.transform(normalized)

'''Example of trained face'''
plt.imshow(model.components_[0].reshape(300,300), cmap='gray')
plt.show()

'''Classifying and restructuring faces'''
faces_pca = PCA(n_components=n_instructors)
faces_pca.fit(normalize(face_vector))
components = faces_pca.transform(face_vector)
projected = faces_pca.inverse_transform(components)
fig, axes = plt.subplots(14,1,figsize=(20,80))
for i, ax in enumerate(axes.flat):
    ax = fig.add_subplot(3, 5, i + 1, xticks=[], yticks=[])
    ax.imshow(projected[i].reshape(300,300),cmap="gray")
plt.show()
