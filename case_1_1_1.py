'''
Case Study 1.1.1 - Genetic Codes
Jake Ullman
'''

import re
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.decomposition import PCA
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_score
from sklearn.preprocessing import StandardScaler


'''Clean data and form into snippets of 300 characters'''
genetic_code = open('ccrescentus.fa').read().splitlines()
# print(genetic_code)
genetic_code_clean = "".join(genetic_code[1:])
# print(genetic_code_clean)
genetic_code_chunked = [genetic_code_clean[i:i+300] for i in range(0, len(genetic_code_clean), 300)]
# print(genetic_code_chunked)

'''Create list of dataframes for frequencies of short words'''
short_words_list = []
for short_word in range(1, 5):
    inner_frame = []
    for chunk in genetic_code_chunked:
        frequencies = {}
        for segment in re.findall("".join(['.{', str(short_word), '}']), chunk):  # finds instances of short word
            frequencies[segment] = frequencies.get(segment, 0) + 1  # add 1 to count of occurrence
            # print(frequencies)
        inner_frame.append(frequencies)
    short_words_list.append(pd.DataFrame(inner_frame))

# print(short_words_list)

'''Preprocessing, perform principal component analysis, and plot findings'''
pca = []
for z in range(4):
    i = short_words_list[z].fillna(0)
    components = PCA(n_components=2)
    pca_df = pd.DataFrame(components.fit_transform(i))
    pca.append(pca_df)
    plt.subplot(2, 2, z+1)
    plt.scatter(pca_df[0], pca_df[1])

plt.suptitle("PCA Plots for Text Fragments")
plt.show()

'''Clustering and plot findings'''
ideal = pca[2]
k_means = KMeans(n_clusters=7).fit(ideal)
k_means_prediction = k_means.predict(ideal)
centers = k_means.cluster_centers_
score = silhouette_score(ideal, k_means.labels_, metric='euclidean')
print ("K-means score: ", score)

plt.scatter(ideal[0], ideal[1], c=k_means_prediction)
plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5);
plt.title('K-Means Clustering')
plt.show()
